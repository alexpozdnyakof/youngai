export interface BookStat {
    pages: number;
}
export interface MonthStat {
    month?: number;
    books: BookStat[];
}
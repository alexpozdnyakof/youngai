import { Author } from './author.interface';
export interface Book {
  Description: string;
  Excerpt: string;
  ID: number;
  PageCount: number;
  PublishDate: string;
  Title: string;
  Authors?: Author[]
  day?: number;
  month?: number;
}
export interface Author {
    FirstName?: string;
    ID?: number;
    IDBook?: number;
    LastName?: string;
}
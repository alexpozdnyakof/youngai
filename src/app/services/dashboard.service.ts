import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { Observable, of, combineLatest, zip, BehaviorSubject } from 'rxjs';
import { Book } from '../types';
import { take, tap, first, switchMap, map, every, shareReplay, groupBy, toArray, mergeMap, concatAll } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  public chartData$: Observable<any[]>;
  public yearsData$: Observable<any[]>;
  public monthData$: Observable<any[]>;
  public books$: Observable<Book[]>;
  public monthLabels = ['jan', 'feb', 'march', 'april', 'may', 'june', 'jule', 'aug', 'sept', 'oct', 'nov', 'dec'];
  public dateRexp = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})\.(\d{3})/;
  constructor(private _dataService: DataService) { }
  public getBooks$(): Observable<Book[]> {
    if (!this.books$) {
      this.books$ = combineLatest(
        this._dataService.getBooks$().pipe(
          map(books => books.slice(0, 4)),
        ),
        this._dataService.getAuthors$())
        .pipe(
          switchMap(data => this.findAuthors(data[0], data[1])),
          shareReplay(1),
        )
    }
    return this.books$;
  }

  public findAuthors(books, authors): Observable<Book[]> {
    books.forEach((bookResult, book) => {
      return bookResult.Authors = authors.filter(author => author.IDBook === bookResult.ID);
    })
    return of(books);
  }

  public getChartData() {
    if (!this.chartData$) {
      this.chartData$ = this._dataService.getBooks$()
        .pipe(
          map(books => books.sort((a, b) => a.PublishDate > b.PublishDate ? 1 : -1)),
          switchMap(book => book),
          map(book => {
            const dateParsed = book.PublishDate.match(this.dateRexp);
            book.day = Number(dateParsed[3]);
            book.month = Number(dateParsed[2]);
            return {
              day: Number(dateParsed[3]),
              month: Number(dateParsed[2]),
              pages: book.PageCount
            };
          }),
          groupBy(book =>
            book.month, b => b
          ),
          mergeMap(group => zip(of(group.key), group.pipe(toArray()))),
          toArray(),
          shareReplay(1)
        )
    }
    return this.chartData$;
  }

  public getYearDataSource$() {
    if (!this.yearsData$) {
      this.yearsData$ = this.getChartData().pipe(
        switchMap(data => this.calculateYearDataSource(data)),
        shareReplay(1),
      )
    }
    return this.yearsData$;
  }
  public getMonthDataSource$() {
    if (!this.monthData$) {
      this.monthData$ = this.getChartData().pipe(
        switchMap(data => this.calculateMonthDataSource(data)),
        shareReplay(1),
      )
    }
    return this.monthData$;
  }

  public getOneMonthDataSource$(label: string) {
    return this.getChartData().pipe(
      switchMap(data => this.calculateOneMonthDataSource(data, label)),
    )
  }
  public calculateOneMonthDataSource(data, label): Observable<any> {
    const labels = [];
    const dataSource = [];
    const index = this.monthLabels.indexOf(label) + 1;
    const month = data.filter(monthItem => Number(monthItem[0]) === index)[0];
    console.log(month);
    month[1].reduce((summary, item, i, arr) => {
      labels.push(`${this.monthLabels[Number(month[0]) - 1]} ${item.day}`)
      dataSource.push(item.pages)
      return summary
    }, 0)
    return of({ labels: labels, dataSource: dataSource });
  }
  public calculateMonthDataSource(data): Observable<any> {
    const labels = [];
    const dataSource = [];
    for (const month of data) {
      month[1].reduce((summary, item, i, arr) => {
        labels.push(`${this.monthLabels[Number(month[0]) - 1]} ${item.day}`)
        dataSource.push(item.pages)
        return summary
      }, 0)
    }
    return of({ labels: labels, dataSource: dataSource })
  }
  private calculateYearDataSource(data): Observable<any> {
    const labels = [];
    const dataSource = [];
    for (const month of data) {
      labels.push(this.monthLabels[Number(month[0]) - 1])
      month[1].reduce((summary, item, i, arr) => {
        summary += item.pages;
        if (i === arr.length - 1) {
          summary = summary / arr.length
          dataSource.push(summary);
        }
        return summary
      }, 0)
    }
    return of({ labels: labels, dataSource: dataSource })
    /*


    return { labels: labels, dataSource: dataSource }
    */
  }


  public calculateChart(books) {
    const dateRexp = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})\.(\d{3})/;
    books = books.sort((a, b) => a.PublishDate > b.PublishDate ? 1 : -1);
    const graphData = books.reduce((graphArray, book, i, arr) => {
      const dateParsed = book.PublishDate.match(dateRexp);
      const month = `${Number(dateParsed[2])}`;
      graphArray[month] = (graphArray[month] || []).concat({ pages: book.PageCount, day: Number(dateParsed[3]) });
      return graphArray;
    }, {});
    return graphData;
  }




  /*
    public compareArrays(books, authors) {
      const bookIDs = books.map(book => book.ID);
      const authorsIDs = authors.map(author => author.IDBook);
      const comparedArray = [];
      bookIDs.forEach((book, bookIndex) => {
        const bookObject = {
          bookI: bookIndex,
          id: book,
          authors: []
        };
        authorsIDs.forEach((bookID, authorIndex) => {
          if (bookID === book) {
            bookObject.authors.push({ index: authorIndex })
          }
        })
        comparedArray.push(bookObject);
      })
      books.forEach((book, bookIndex) => {
        const authorsInHashmap = comparedArray[bookIndex].authors.map(author => {
          return authors[author.index]
        });
        book.Authors = authorsInHashmap;
      })
      this.books = of(books)
    }*/
}

import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, combineLatest, of } from "rxjs";
import { Book } from "../types";
import { Author } from '../types/author.interface';
import { shareReplay } from 'rxjs/operators';
const CACHE_SIZE = 1;

@Injectable({
  providedIn: "root"
})
export class DataService {
  private settings = {
    customHeaders: new HttpHeaders({ "Content-Type": "application/xml" }),
    url: "https://fakerestapi.azurewebsites.net/api/"
  };

  public books$: Observable<Book[]>;
  public authors$: Observable<Author[]>;

  constructor(private http: HttpClient) {
  }

  getBooks$() {
    if (!this.books$) {
      this.books$ = this.reqBooks().pipe(shareReplay(1))
    }
    return this.books$;
  }
  getAuthors$() {
    if (!this.authors$) {
      this.authors$ = this.reqAuthors().pipe(shareReplay(1))
    }
    return this.authors$;
  }

  reqBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(
      `${this.settings.url}/Books`,
      { headers: this.settings.customHeaders }
    );
  }
  reqAuthors(): Observable<Author[]> {
    return this.http.get<Author[]>(
      `${this.settings.url}/Authors`,
      { headers: this.settings.customHeaders }
    );
  }

  getBook(id): Observable<Book> {
    return this.http.get<Book>(
      `${this.settings.url}/Books/${id}`,
      { headers: this.settings.customHeaders }
    );
  }
}

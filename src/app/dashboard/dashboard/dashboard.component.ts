import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { Book } from 'src/app/types';
import { Observable, of, } from 'rxjs';
import { tap, } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { DashboardService } from 'src/app/services/dashboard.service';
import { ChartDirective } from 'src/app/shared/chart.directive';
import { Router } from '@angular/router';
@Component({
  selector: 'yai-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(ChartDirective, { static: true }) chart: ChartDirective;
  public context: CanvasRenderingContext2D

  public books$: Observable<Book[]>;
  public chartMode: String = 'year';
  public isAuthors = false;
  public chartData$$: Observable<any>;
  public labels$$: Observable<[]>;
  constructor(private dashboardService: DashboardService, private router: Router) {
    this.chartData$$ = this.dashboardService.getYearDataSource$().pipe(
      tap((data: any) => this.labels$$ = of(data.labels)),
      untilDestroyed(this)
    );
    this.books$ = this.dashboardService.getBooks$();
  }

  ngAfterViewInit() { }
  ngOnInit() { }

  public toggleChartMode(mode) {
    this.chartMode = mode;
    switch (mode) {
      case 'year':
        return this.chartData$$ = this.dashboardService.getYearDataSource$();
      case 'month':
        return this.chartData$$ = this.dashboardService.getMonthDataSource$()
      default:
        return this.chartData$$ = this.dashboardService.getOneMonthDataSource$(mode)
    }
  }
  public navToBook(e, id) {
    if (e.target.tagName.toLowerCase() === 'button') { return };
    this.router.navigate(['/books', id])
  }
  public toggleAuthors() {
    this.isAuthors = !this.isAuthors;
  }

  ngOnDestroy() { }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookViewRoutingModule } from './book-view-routing.module';
import { BookViewComponent } from './book-view/book-view.component';


@NgModule({
  declarations: [BookViewComponent],
  imports: [
    CommonModule,
    BookViewRoutingModule
  ]
})
export class BookViewModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookViewComponent } from './book-view/book-view.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: ':id',
        component: BookViewComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookViewRoutingModule { }

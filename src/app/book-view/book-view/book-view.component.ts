import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Book } from 'src/app/types';
import { switchMap } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'yai-book-view',
  templateUrl: './book-view.component.html',
  styleUrls: ['./book-view.component.scss']
})
export class BookViewComponent implements OnInit, OnDestroy {
  public excrept = false;
  constructor(private dataService: DataService, private router: Router, private route: ActivatedRoute) { }
  public book$: Observable<Book>;
  ngOnInit() {
    this.book$ = this.route.params.pipe(
      switchMap(params => this.dataService.getBook(params['id'])),
      untilDestroyed(this)
    )
  }
  ngOnDestroy() { }
  goBack() {
    this.router.navigate(['/'])
  }
}

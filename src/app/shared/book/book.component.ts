import { Component, OnInit, Input, ChangeDetectionStrategy, ViewEncapsulation } from "@angular/core";
import { Book } from "src/app/types";

@Component({
  selector: "yai-book",
  templateUrl: "./book.component.html",
  styleUrls: ["./book.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BookComponent implements OnInit {
  @Input() book: Book;
  @Input() isAuthors = false;
  constructor() { }

  ngOnInit() { }
}

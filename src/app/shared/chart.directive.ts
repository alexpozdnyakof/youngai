import { Directive, ElementRef, OnInit, AfterViewInit, OnDestroy, Input } from '@angular/core';
import { Chart } from 'chart.js';

export interface ChartData {
  labels?: [];
  dataSource?: [];
}
@Directive({
  selector: '[yaiChart]'
})
export class ChartDirective implements OnInit, AfterViewInit, OnDestroy {

  constructor(private el: ElementRef) { }
  private context: CanvasRenderingContext2D;
  private _dataSource: ChartData = {};
  public chart;
  @Input() set dataSource(data) {
    if (data) {
      this._dataSource = data;
      this.updateChart();
    }
  }
  get host() {
    return this.el.nativeElement;
  }
  get dataSource() {
    return this._dataSource;
  }
  ngAfterViewInit() {
    this.context = (<HTMLCanvasElement>this.host).getContext('2d');
    if (!this.chart) {
      this.updateChart();
    }
  }
  ngOnInit() {
  }
  ngOnDestroy() {
  }

  public updateChart() {
    if (!this.context) { return }
    if (!this.chart) {
      return this.makeChart(this.dataSource.labels, this.dataSource.dataSource)
    }
    this.chart.data.datasets.splice(0, 1);
    const data = {
      data: this.dataSource.dataSource,
      borderColor: "#3cba9f",
      fill: false
    }
    this.chart.data.datasets.push(data);
    this.chart.data.labels = this.dataSource.labels;
    return this.chart.update();
  }
  public makeChart(labels, dataSource) {
    this.chart = new Chart(this.context, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [
          {
            data: dataSource,
            borderColor: "#3cba9f",
            fill: false
          },
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
  }

}

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BookComponent } from "./book/book.component";

import { OverlayModule } from '@angular/cdk/overlay';
import { ChartDirective } from './chart.directive';

@NgModule({
  declarations: [BookComponent, ChartDirective],
  imports: [CommonModule, OverlayModule],
  exports: [BookComponent, ChartDirective],
})
export class SharedModule { }

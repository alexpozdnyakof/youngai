import { Component } from '@angular/core';

@Component({
  selector: 'yai-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'youngai';
}
